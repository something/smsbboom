package main

type Api struct {
	Desc   string            `json:"desc"`
	Url    string            `json:"url"`
	Method string            `json:"method"`
	Header map[string]string `json:"header"`
	Data   map[string]string `json:"data"`
	Form   string            `json:"form"`
}
