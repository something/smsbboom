# SmsBBoom
    仅供测试使用
# Use
| 参数名     | 解释                               |
| ---------- | ---------------------------------- |
| --phone    | 手机号码 以英文逗号分隔（`,`）多个 |
| --loop     | 循环次数                           |
| --duration | 每次循环间隔                       |
| --proxy    | 设置代理，通过代理来发送请求       |

## 预构建版
[下载地址](https://gitea.com/something/smsbboom/releases/tag/pre01)
### Windows
```bash
./smsboom_win_amd64 --phone=13499990000,15599998888 --loop=3 --duration=1h
```
### MacOS
```bash
chmod +x smsbboom_mac_arm64
./smsbboom_mac_arm64 --phone=13499990000,15599998888 --loop=3 --duration=1h
```

## 源码编译
```bash 
# 每个小时循环一次共三次并且使用代理发送请求
go run main.go --phone=13499990000,15599998888 --loop=3 --duration=1h --proxy="http://127.0.0.1:7890"
```

# Announce
1.禁止拷贝本项目。
2.禁止用于违法用途。
3.禁止贩卖与销售开源代码。
4.任何法律风险由违法使用着承担。