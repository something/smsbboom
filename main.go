package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"log/slog"
	"os"
	"strings"
	"sync"
	"text/template"
	"time"

	"github.com/imroc/req/v3"
)

type Data struct {
	Username string
	Time     string
	Phone    string
}

func main() {
	var loop int
	var phone, proxy, duration string

	flag.StringVar(&proxy, "proxy", "", "proxy")
	flag.StringVar(&phone, "phone", "", "phone number")
	flag.StringVar(&duration, "duration", "1s", "interval")
	flag.IntVar(&loop, "loop", 1, "loop times")
	flag.Parse()
	if phone == "" {
		slog.Error("phone number required")
		return
	}
	dura, err := time.ParseDuration(duration)
	if err != nil {
		slog.Error("duration parse error", err)
		return
	}

	httpClient := req.NewClient()
	if proxy != "" {
		httpClient.SetProxyURL(proxy)
	}

	httpClient.SetCommonHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 Edge/16.16299")
	httpClient.SetCommonHeader("Accept", "application/json")
	httpClient.SetCommonHeader("Accept-Language", "en-US,en;q=0.8")
	httpClient.SetCommonHeader("Cache-Control", "max-age=0")
	httpClient.SetCommonHeader("Upgrade-Insecure-Requests", "1")

	file, err := os.ReadFile("apis.json")
	if err != nil {
		slog.Error("apis.json read error", err)
	}
	phones := strings.Split(phone, ",")

	ticker := time.NewTicker(dura)
	for i := 0; i < loop; i++ {
		wg := &sync.WaitGroup{}
		for _, v := range phones {
			wg.Add(1)
			data := Data{RandomString(6), time.Now().Format(time.DateTime), v}
			go Boom(wg, httpClient, file, data, dura)
		}
		wg.Wait()
		<-ticker.C
	}
}

func Boom(wg *sync.WaitGroup, httpClient *req.Client, file []byte, data Data, d time.Duration) {
	defer wg.Done()
	t := template.Must(template.New("json").Parse(string(file)))
	var jsonBytes bytes.Buffer
	if err := t.Execute(&jsonBytes, data); err != nil {
		slog.Error("template execute error", err)
		return
	}

	var apis []Api
	if err := json.Unmarshal(jsonBytes.Bytes(), &apis); err != nil {
		slog.Error("json unmarshal error", err)
		return
	}

	for i, v := range apis {
		c := httpClient.R()

		if len(v.Header) != 0 {
			c.SetHeaders(v.Header)
		}
		var res *req.Response
		var err error
		if v.Method == "get" {
			res, err = c.Get(v.Url)
		}
		if v.Method == "post" {
			switch c.Headers.Get("Content-Type") {
			case "application/x-www-form-urlencoded":
				c.SetFormData(v.Data)
			default:
				c.SetBody(v.Data)
			}
			res, err = c.Post(v.Url)
		}
		slog.Info(v.Desc, "index", i, "res", string(res.Bytes()), "err", err)
	}
}
